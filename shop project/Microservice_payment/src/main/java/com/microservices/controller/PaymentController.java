package com.microservices.controller;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.microservices.Entity.Payment;
import com.microservices.service.PaymentService;

@RestController
@RequestMapping("/payment")
public class PaymentController {

	@Autowired
	private PaymentService service;
	
	
	@PostMapping("/dopayment")
	public Payment dopayment(@RequestBody Payment payment) {
	 service.doPayment(payment);
	 return payment;
	}
	

}
