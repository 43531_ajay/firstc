package com.micro.common;
import com.micro.Entity.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;



public class TransactionRequest {
	private Order order;
	private Payment payment;
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public Payment getPayment() {
		return payment;
	}
	public void setPayment(Payment payment) {
		this.payment = payment;
	}
;
	public TransactionRequest(Order order, Payment payment) {

		this.order = order;
		this.payment = payment;
	}
	
	public TransactionRequest() {

		}
	
}
