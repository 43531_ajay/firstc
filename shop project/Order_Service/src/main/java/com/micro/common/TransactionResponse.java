package com.micro.common;

import com.micro.Entity.Order;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;



public class TransactionResponse {
private Order order;
private double amount;
private String transactionId;
public TransactionResponse(Order order, double amount, String transactionId, String messsage) {

	this.order = order;
	this.amount = amount;
	this.transactionId = transactionId;
	this.messsage = messsage;
}
private String messsage;
public TransactionResponse() {

	
}
public String getMesssage() {
	return messsage;
}
public void setMesssage(String messsage) {
	this.messsage = messsage;
}
public Order getOrder() {
	return order;
}
public void setOrder(Order order) {
	this.order = order;
}
public double getAmount() {
	return amount;
}
public void setAmount(double amount) {
	this.amount = amount;
}
public String getTransactionId() {
	return transactionId;
}
public void setTransactionId(String transactionId) {
	this.transactionId = transactionId;
}

}
