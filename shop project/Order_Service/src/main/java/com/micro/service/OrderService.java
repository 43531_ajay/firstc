package com.micro.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.micro.Entity.Order;
import com.micro.Repository.OrderRepository;
import com.micro.common.Payment;
import com.micro.common.TransactionRequest;
import com.micro.common.TransactionResponse;

@Service
public class OrderService {

	@Autowired
	private OrderRepository repository;
	
	@Autowired
	private RestTemplate template;
	
	public TransactionResponse bookorder(TransactionRequest request ){
		
		String response ="";
		Order order =request.getOrder();
		Payment payment =request.getPayment();
		payment.setOrderId(order.getId()); 
		payment.setAmount(order.getPrice());
		
		Payment paymentResponse= template.postForObject("http://localhost:9091/payment/dopayment", payment, Payment.class);
		 //rest call 
		response =paymentResponse.getPaymentStatus().equals("success")?"payment processing success and  palce": "there is a  failure and order added to cart";
		return new TransactionResponse(order,paymentResponse.getAmount() ,paymentResponse.getTransactionId(),response);
		
		
		
	}
}
