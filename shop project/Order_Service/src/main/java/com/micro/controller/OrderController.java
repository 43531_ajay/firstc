package com.micro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.micro.Entity.Order;
import com.micro.common.Payment;
import com.micro.common.TransactionRequest;
import com.micro.common.TransactionResponse;
import com.micro.service.OrderService;

@RequestMapping("/order")
@RestController
public class OrderController {

	@Autowired
	private OrderService service;
	
	
	@PostMapping("/orders")
	public TransactionResponse shop(@RequestBody TransactionRequest request) {
		
		
		 return service.bookorder(request);
		//do a rest call to payment and provide order id
	}
}
