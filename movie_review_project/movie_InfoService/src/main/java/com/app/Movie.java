package com.app;

public class Movie {
private String name;
private String movieId;
public String getName() {
	return name;
}
public Movie() {

}
public void setName(String name) {
	this.name = name;
}
public Movie(String name, String movieId) {
	super();
	this.name = name;
	this.movieId = movieId;
}
public String getMovieId() {
	return movieId;
}
public void setMovieId(String movieId) {
	this.movieId = movieId;
}

}
	

