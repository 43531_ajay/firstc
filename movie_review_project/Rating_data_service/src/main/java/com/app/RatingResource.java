package com.app;
import com.app.Ratings;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ratingsdata")
public class RatingResource {

	
	@RequestMapping("/{movieId}")
	public List<Ratings> getCatalog(@PathVariable("movieId")String userId){
		return Collections.singletonList(
			new Ratings("transformers",1));
	}
		
		@RequestMapping("/users/{userId}")
		public UserRating getRatings(@PathVariable("userId")String userId){
			List<Ratings> rating = Arrays.asList(
				new Ratings("transformers",8),
				new Ratings("dust to dawn",9));
			UserRating ur=new UserRating();
			ur.setUserRating(rating);
			return ur;
	}
}