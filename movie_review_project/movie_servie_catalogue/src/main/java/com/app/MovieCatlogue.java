package com.app;
import com.app.CatalogItem;
import com.app.Ratings;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/catalog")
public class MovieCatlogue {
	  @Autowired
	    private RestTemplate restTemplate;
	//get all rated movie Ids
	@RequestMapping("/{userId}")
	public List<CatalogItem> getCatalog(@PathVariable("userId") String userId){ // breaking our own principle producing list on api call

	
	UserRating ratings = restTemplate.getForObject("http://localhost:8083/ratingsdata/users/" + userId, UserRating.class );
        return	ratings.getUserRating().stream().map(rating->{
        	
        	//for each movie Id ,call movie info service and get details 
       Movie movie= restTemplate.getForObject( "http://localhost:8082/movies/foo"+rating.getMovieId(),Movie.class);
        		
     //put them all together
       return new CatalogItem(movie.getName(),"Desc",rating.getRating());
        				})
        		 .collect(Collectors.toList());

	
	
	 
	  
	
	
	
  
	
}
}
